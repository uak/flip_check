# Flip Check or FlipFetch

Get information about Flipstarter campaigns using Telegram.

It uses [flipstarters.bitcoincash.network/](https://flipstarters.bitcoincash.network/#/) data to show information about Flipstarter.

* Running at [@FlipFetchBot](https://t.me/FlipFetchBot)

## Screenshots

![bot screen recording](images/flip_fetch_screen_recording.gif)

## Bot Commands

 * `/active` to list active campaigns
 * `/recent` to list recently funded campaigns
 * `/recent [number]` Display campaigns based on page number
 * `/camp [number]` to get detailed campaign info by campaign index number 
 * `/add` to get info on adding a campaign to the list
 * `/create` to get info on launching your own campaign
 
## Running the bot

Dependencies are specified in `pyproject.toml`, You can install them using `poetry install` then run:

```
cd src/
python flip_bot.py
```

## Using Docker

### Build the Container

```
docker build -t flipcheck_docker:first .
```

Where `flipcheck_docker` is the name of we assign to the container and `first` is the tag.

### Setup Telegram Token

Copy `env.sample` to `.env` and edit your variables like Telegram token.  

### Start the Container

We run docker specifiying the environment file:

```
docker run --env-file flip_check/src/.env -it flipcheck_docker:first
```

Please note that docker have issues with quote symbol (") in `.env` file so your file shouldn't have it while using docker.

### Issues

Cron in docker is still WIP, campaign wouldn't update automatically if source change.

## License

* AGPL v3

