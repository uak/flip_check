FROM python:3.11-alpine

RUN adduser -S flipcheck
USER flipcheck
WORKDIR /home/flipcheck
COPY --chown=flipcheck:flipcheck . .
RUN pip3 install --default-timeout=1000 --upgrade pip
RUN pip3 install --default-timeout=1000 -r requirements.txt
CMD cd src && python3 check_download_new.py && python3 flip_bot.py
