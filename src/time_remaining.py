# https://stackoverflow.com/a/54701179

import logging
import datetime

# logging.basicConfig(level=logging.DEBUG)

seconds_in_hour = 3600
seconds_in_minute = 60


def countdown(start, stop):
    start_time = datetime.datetime.utcfromtimestamp(start)
    end_time = datetime.datetime.utcfromtimestamp(stop)
    difference = end_time - start_time
    count_hours, rem = divmod(difference.seconds, seconds_in_hour)
    count_minutes, count_seconds = divmod(rem, seconds_in_minute)
    if difference.days == 1:
        return str(difference.days) + " day"
    elif difference.days != 0:
        return str(difference.days) + " days"
    else:
        return str(count_hours) + " h " + str(count_minutes) + " m."
