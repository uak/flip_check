"""
Get Flipstarter campagins info
Based on aiogram bot

It uses this API: https://flipbackend.bitcoincash.network/v1/flipstarter/
"""

import asyncio
import logging
import sys
import traceback
from os import getenv
import peewee

from aiogram import Bot, Dispatcher, html
from aiogram.client.default import DefaultBotProperties
from aiogram.enums import ParseMode
from aiogram.filters import CommandStart, Command
from aiogram.types import Message

from get import HandleRemoteAPI
from dotenv import load_dotenv

import threading
import time
import schedule
from pathlib import Path

from strings import (
    welcome_text,
    extra_active_text,
    no_active_text,
    create_text,
    add_command_text,
)

logger = logging.getLogger(__name__)

load_dotenv()

log_level = getenv("log_level")

if log_level == "debug":
    logger.setLevel(logging.DEBUG)
    handler = logging.StreamHandler()
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    )
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.debug("Debug log level")


api_class = HandleRemoteAPI()

TOKEN = getenv("API_TOKEN")
data_file = getenv("data_file")
api_url = getenv("api_url")
data_file = getenv("data_file")
data_dir = getenv("data_dir")
quick_storage_file = getenv("quick_storage_file")
running_campaigns_file = getenv("running_campaigns_file")


# Make datadir if doesn't exist
Path(data_dir).mkdir(parents=True, exist_ok=True)

# All handlers should be attached to the Router (or Dispatcher)
dp = Dispatcher()


def handle_updates():
    api_class.get_the_main_api_updated(
        api_url, quick_storage_file, "Content-Length", data_file
    )
    logger.info("Got the main API checked")
    api_class.get_running_more_data(data_file, running_campaigns_file, "ETag")
    logger.info("Got the running campaigns checked")


try:
    logger.info("Trying to load required files from API")
    handle_updates()
except Exception as e:
    logger.error(f"unable to fetch required files!\n{e}")
    # ~ exit("unable to call API")


@dp.message(CommandStart())
async def command_start_handler(message: Message) -> None:
    """
    This handler receives messages with `/start` command
    """
    text = welcome_text
    await message.answer(text, parse_mode="markdown", disable_web_page_preview=True)


@dp.message(Command("create"))
async def create(message: Message):
    """
    This handler will be called when user sends `/create` command
    """
    text = create_text
    logger.info("create campaign requested!")
    await message.reply(text, parse_mode="markdown", disable_web_page_preview=True)


@dp.message(Command("add"))
async def add(message: Message, command):
    try:
        # ~ store_campaign_json(command.args)
        text = add_command_text
        logger.info("add campaign requested!")
        await message.answer(text)
    except Exception as e:
        logger.error(f"Exception: {e}")
        traceback.print_exc()
        text = "there was an issue with adding the campaign"
        await message.answer(text)


@dp.message(Command("active"))
async def active(message: Message, command):
    try:
        data = api_class.load_local_data(data_file)
        indexed_data = api_class.index_data(data)
        running_campaigns = api_class.get_running_campaigns(indexed_data)
        number_of_campagins = len(running_campaigns)
        if running_campaigns and number_of_campagins < 6:
            last_5 = api_class.get_last_5(running_campaigns, 0, 5)
            text = api_class.get_basic_info(last_5, "active")
            text = text + extra_active_text
        elif number_of_campagins >= 6:
            text = api_class.get_basic_info(running_campaigns, "active")
            text = text + extra_active_text
        else:
            text = no_active_text
        logger.info("Active campaigns requested!")
        await message.answer(text, parse_mode="markdown", disable_web_page_preview=True)
    except Exception as e:
        traceback.print_exc()
        text = f"there was an issue with listing the campaigns\n {e}"
        await message.answer(text)


@dp.message(Command("recent"))
async def recent(message: Message, command):
    try:
        if not command.args:
            data = api_class.load_local_data(data_file)
            indexed_data = api_class.index_data(data)
            successful_campaigns = api_class.get_successful_campaigns(indexed_data)

            number_of_campagins = len(successful_campaigns)
            chunks = number_of_campagins / 5

            last_5 = api_class.get_last_5(successful_campaigns, 0, 5)
            text = api_class.get_basic_info(last_5, "successful")
            extra_text = f"""\nWe have a total **{number_of_campagins}** recorded successful campaigns!\
            \nThey are split into {int(chunks)} pages. You can review by adding page number to command. example: `/recent 3`"""
            logger.info("recent campaigns requested!")
            text = text + extra_text
            await message.answer(
                text, parse_mode="markdown", disable_web_page_preview=True
            )
        else:
            if command.args.isdigit():
                number = int(command.args)
                data = api_class.load_local_data(data_file)
                campaigns = api_class.get_successful_campaigns(data)
                indexed_data = api_class.index_data(campaigns)
                number_of_campagins = len(indexed_data)
                end = number + 5
                last_5 = api_class.get_last_5(indexed_data, number, end)
                text = api_class.get_basic_info(last_5, "successful")
                text = (
                    text
                    + f"\nWe have a total **{number_of_campagins}** recorded successful campaigns!\
                    \nPage: {number}"
                )
                await message.answer(
                    text, parse_mode="markdown", disable_web_page_preview=True
                )
            elif isinstance(command.args, str):
                text = "You should use numbers not strings"
                await message.answer(text)
    except Exception as e:
        logger.error(f"Exception: {e}")
        traceback.print_exc()
        text = "there was an issue with listing the campaigns"
        await message.answer(text)


@dp.message(Command("camp"))
async def camp(message: Message, command):
    """Get campaign info when /camp is issued."""
    try:
        data = api_class.load_local_data(data_file)
        campaign_data = data[int(command.args)]
        arg_is_digit = True
    except Exception as e:
        arg_is_digit = False
        logger.error(f"camp: User did not use digit {e}")
    try:
        if arg_is_digit:
            text = api_class.get_info(campaign_data)
            await message.answer(
                text, parse_mode="markdown", disable_web_page_preview=True
            )
        else:
            await message.answer(
                "Please use digits only!\ne.g., `/camp 170`",
                parse_mode="markdown",
                disable_web_page_preview=True,
            )
    except peewee.DoesNotExist:
        await message.answer("problem: DoesNotExist", parse_mode="markdown")
    # ~ except CampaignsDoesNotExist:
    # ~ await message.answer("problem", parse_mode='markdown')
    except Exception as e:
        traceback.print_exc()
        logger.error(f"Exception: {e}")
        await message.answer(f"Problem: {e}", parse_mode="markdown")


def run_continuously(interval=1):
    """Continuously run, while executing pending jobs at each
    elapsed time interval.
    @return cease_continuous_run: threading. Event which can
    be set to cease continuous run. Please note that it is
    *intended behavior that run_continuously() does not run
    missed jobs*. For example, if you've registered a job that
    should run every minute and you set a continuous run
    interval of one hour then your job won't be run 60 times
    at each interval but only once.
    """
    cease_continuous_run = threading.Event()

    class ScheduleThread(threading.Thread):
        @classmethod
        def run(cls):
            while not cease_continuous_run.is_set():
                schedule.run_pending()
                time.sleep(interval)

    continuous_thread = ScheduleThread()
    continuous_thread.start()
    return cease_continuous_run


def update_local_data():
    """
    Call the directory API to update local data and call each active
    campaign own API at `campaign/1`
    """
    api_class.get_the_main_api_updated(
        api_url, quick_storage_file, "Content-Length", data_file
    )
    logger.info("Got the main API checked")
    api_class.get_running_more_data(data_file, running_campaigns_file, "ETag")
    logger.info("Got the running campaigns checked")


schedule.every(600).seconds.do(handle_updates)

stop_run_continuously = run_continuously()


@dp.message()
async def echo(message: Message):
    # old style:
    # await bot.send_message(message.chat.id, message.text)

    await message.answer(message.text)


async def main() -> None:
    # Initialize Bot instance with default bot properties which will be passed to all API calls
    bot = Bot(token=TOKEN, default=DefaultBotProperties(parse_mode=ParseMode.HTML))

    # And the run events dispatching
    await dp.start_polling(bot)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, stream=sys.stdout)
    asyncio.run(main())
