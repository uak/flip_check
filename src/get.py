import requests
import logging
import hashlib
import shelve
import json
import time
import os

from time_remaining import countdown

from urllib.parse import urlparse, urljoin
from dotenv import load_dotenv

load_dotenv()

log_level = os.environ["log_level"]

# Handling logs
logger = logging.getLogger(__name__)

if log_level == "debug":
    logger.setLevel(logging.DEBUG)
    handler = logging.StreamHandler()
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)



W = "\033[0m"  # white (normal)
R = "\033[31m"  # red
G = "\033[32m"  # green
Or = "\033[33m"  # orange
B = "\033[34m"  # blue
P = "\033[35m"  # purple


quick_storage_file = os.environ["quick_storage_file"]
api_url = os.environ["api_url"]
data_file = os.environ["data_file"]
data_dir = os.environ["data_dir"]


# ~ print(R+"Execution time:"+W, "text", 'seconds')


def get_by_key_value(data, key, value):
    """
    Get data entry based on key and value
    """
    return [d for d in data if d[key] == value]


def get_by_key_value_indexed(data, key, value):
    """
    Get data entry based on key and value and append index
    """
    # ~ indexed_data = index_data(data)
    return [d for d in data if d[key] == value]


def satoshi_to_bitcoin(satoshi):
    return satoshi / 100000000


class HandleRemoteAPI:
    """
    Handle remote request and provide latest data
    """

    def get_remote_file(self, api_url):
        try:
            logger.debug(f"get_remote_file: requesting {api_url}")
            return requests.get(api_url)
        except Exception as e:
            logger.debug(f"get_remote_file: Exception: {e}")

    def get_hash(self, data):
        """Gets sha256 hashing of data"""
        return hashlib.sha256(data).hexdigest()

    def save_key_shelve(self, quick_storage_file, key, value):
        with shelve.open(quick_storage_file) as shelf:
            shelf[key] = value

    # ~ def save_hash(self, quick_storage_file, _hash):
    # ~ with shelve.open(quick_storage_file) as shelf:
    # ~ shelf['hash'] = _hash
    # ~ shelf['time'] = time.time()
    # ~ logger.debug(f"time: {time.time()}")
    # ~ logger.debug(f"hash: {_hash}")

    def get_remote_header(self, api_url):
        try:
            logger.debug("get_remote_header: requesting remote headers")
            return requests.head(api_url)
        except Exception as e:
            logger.debug(f"get_remote_header: Exception: {e}")

    def get_data_shelve(self, quick_storage_file, key):
        with shelve.open(quick_storage_file) as shelf:
            return shelf[key]

    def outdated_file(self, quick_storage_file, period, last_checked_time):
        """
        Check if file is outdated
        """
        with shelve.open(quick_storage_file) as shelf:
            last_time = shelf["time"]
            gab = time.time() - last_time
            if gab > period * 100 * 60:
                logger.debug("outdated_file: Outdated file")
                return True
            else:
                return False
            logger.debug("outdated_file: file is recent")

    def load_local_data(self, data_file):
        with open(data_file, "r") as handle:
            logger.debug("load_local_data: " + R + "Local file loaded." + W)
            return json.load(handle)

    def load_local_file(self, file_name):
        with open(file_name, "r") as handle:
            logger.debug("load_local_file " + R + "Local file loaded." + W)
            return handle.read()

    def index_data(self, data):
        new_list = []
        for index, data_record in enumerate(data):
            data_record["index"] = index
            new_list.append(data_record)
        logger.debug("index_data: new list")
        return new_list

    def store_remote_json(self, data_file, remote_data):
        logger.debug(f"store_remote_json: data_file: {data_file}")
        logger.debug(f"store_remote_json: remote_data: {remote_data}")
        with open(data_file, "wb") as handle:
            handle.write(remote_data.content)
            # ~ logger.debug(f"hash: {remote_file_hash}")
            logger.debug("store_remote_json: New remote file hash saved in pickle file")

    def compare_with_remote(self, remote_file_hash, current_hash):
        try:
            return remote_file_hash == current_hash
            logger.debug(f"compare_with_remote: hash: {remote_file_hash}")
            logger.debug("compare_with_remote: File not changed in API.")
        except Exception as e:
            raise e

    def get_running_campaigns(self, data):
        running_campaigns_list = []
        for i in get_by_key_value(data, "status", "running"):
            running_campaigns_list.append(i)
        return running_campaigns_list

    def get_successful_campaigns(self, data):
        successful_campaigns_list = []
        for i in get_by_key_value(data, "status", "success"):
            successful_campaigns_list.append(i)
        return successful_campaigns_list

    def get_expired_campaigns(self, data):
        expired_campaigns_list = []
        for i in get_by_key_value(data, "status", "expired"):
            expired_campaigns_list.append(i)
        return expired_campaigns_list

    def get_running_campaigns_indexed(self, data):
        running_campaigns_list = []
        for i in get_by_key_value_indexed(data, "status", "running"):
            running_campaigns_list.append(i)
        # ~ with open("test_running_campaigns_list.json", "w+") as f:
            # ~ json.dump(running_campaigns_list, f)
        return running_campaigns_list

    def get_successful_campaigns_indexed(self, data):
        successful_campaigns_list = []
        for i in get_by_key_value_indexed(data, "status", "success"):
            successful_campaigns_list.append(i)
        # ~ with open("test_successful_campaigns_list.json", "w+") as f:
            # ~ json.dump(successful_campaigns_list, f)
        return successful_campaigns_list

    def get_expired_campaigns_indexed(self, data):
        expired_campaigns_list = []
        for i in get_by_key_value_indexed(data, "status", "expired"):
            expired_campaigns_list.append(i)
        # ~ with open("test_expired_campaigns_list.json", "w+") as f:
            # ~ json.dump(expired_campaigns_list, f)
        return expired_campaigns_list

    def get_campaign_data_by_index(self, index, data):
        return data[index]

    def get_basic_info(self, data, campaign_type):
        text = "✨ Campaigns ✨\n\n"
        for i in data:
            title = i["title"]
            url = i["url"]
            base_url, file_name = self.url_to_filename(url)
            logger.debug(f"get_basic_info: File name: {file_name}")
            if campaign_type == "active":
                try:
                    direct_data = self.load_local_data(data_dir + file_name)
                    expires_epoch = direct_data["campaign"]["expires"]
                    expires = countdown(time.time(), expires_epoch)
                except Exception as e:
                    logger.error(f"get_basic_info: Unable to open file. Exception: {e}")
                    pass
            index = i["index"]
            amount = satoshi_to_bitcoin(i["amount"])
            try:
                b = f"""🔥 [{title}]({url})\
                        \n**💰 Requested:** {amount}\
                        \n**⌛ Expires in:** {expires}\
                        \n**🗂️ Index:** {index}\
                        \n\n"""
            except Exception as e:
                b = f"""🔥 [{title}]({url})\
                    \n**💰 Requested:** {amount}\
                    \n**🗂️ Index:** {index}\
                    \n\n"""
                logger.debug(f"get_basic_info: Was unable to get expiry {e}")
            text += b
        return text

    def get_info(self, data):
        text = ""
        logger.debug(f"get_info: {data}")
        title = data.get("title")
        url = data.get("url")
        description = data.get("description")
        funded_tx = data.get("funded_tx")
        archives = data.get("archives")
        status = data.get("status")
        announcements = data.get("announcements")
        screenshots = data.get("screenshots")
        amount = satoshi_to_bitcoin(data.get("amount"))
        logger.debug(f"get_info: Record: {data}")
        text = f"""🔥 Campaign: [{title}]({url})\
                \n**💰 Requested:** {amount}\
                \n**🏳️ status:** {status}\
                \n**🗒️** {description}"""
        if len(funded_tx) >= 42:
            text = (
                text
                + f"""\n**🧾 funded tx:** `{funded_tx}` [🌐](https://blockchair.com/bitcoin-cash/transaction/{funded_tx})"""
            )
        if archives:
            archives_text = "\n**💾 Archives**: "
            for i, v in enumerate(archives, 1):
                archives_text = archives_text + f'[{i}]({v["url"]}) '
            text = text + archives_text
        if announcements:
            announcements_text = "\n**📢 Announcements**: "
            for i, v in enumerate(announcements, 1):
                announcements_text = announcements_text + f'[{i}]({v["url"]}) '
            text = text + announcements_text
        if screenshots:
            screenshots_text = "\n**📷 Screenshots**: "
            for i, v in enumerate(screenshots, 1):
                screenshots_text = screenshots_text + f'[{i}]({v["image"]}) '
            text = text + screenshots_text

        return text

    def get_x_running_campaigns(self, data):
        text = ""
        for i in get_by_key_value(data, "status", "running"):
            title = i["title"]
            url = i["url"]
            amount = i["amount"]
            b = f"""🔥 **Campaign**\
                    \n✨ [{title}]({url})\
                    \n**💰 Requested:** {amount}\
                    \n\n"""
            text += b
            logger.info("get_x_running_campaigns: Sent campain list as markdown")
        return text

    def get_last_5(self, data, start, end):
        dict_me = []
        for x in list(reversed(list(data)))[start:end]:
            dict_me.append(x)
        return dict_me

    def get_the_main_api_updated(
        self, api_url, quick_storage_file, header_type, data_file
    ):
        response = self.get_remote_header(api_url)
        try:
            stored_content_length = self.get_data_shelve(quick_storage_file, api_url)
        except KeyError as e:
            logger.error(f"get_the_main_api_updated: Key Error: {e}")
            stored_content_length = ""
        except Exception as e:
            raise e

        # FIXME - fast fix for issue with one campaign
        try:
            content_length = response.headers[header_type]
        except Exception as e:
            content_length = ""
            logger.error(f"Issue with fetching content header {e}")

        if (content_length == stored_content_length) and (content_length != ""):
            logger.info("get_the_main_api_updated: Remote file size matches local one")
            pass
        else:
            logger.debug("get_the_main_api_updated: Remote file size does not match local one")
            remote_data = self.get_remote_file(api_url)
            self.store_remote_json(data_file, remote_data)
            logger.debug(f"Remote json was saved as {data_file}")

            self.save_key_shelve(quick_storage_file, api_url, content_length)

    def get_base_url(self, url):
        return urljoin(url, ".")

    def campaign_api_url_from_base(self, base_url):
        return base_url + "campaign/1/"

    def url_to_filename(self, url):
        base_url = self.get_base_url(url)
        o = urlparse(base_url)
        return base_url, o.netloc + ".json"

    def get_running_more_data(self, data_file, running_campaigns_file, header_type):
        data = self.load_local_data(data_file)
        indexed_data = self.index_data(data)
        running_campaigns = self.get_running_campaigns(indexed_data)
        for i in running_campaigns:
            campaign_url = i["url"]
            base_url, file_name = self.url_to_filename(campaign_url)
            try:
                campaign_api_url = self.campaign_api_url_from_base(base_url)
                file_full_path = data_dir + file_name
                logger.debug(f"get_running_more_data: file_name: {file_name}")
            except:
                error_msg = f"Issue with one campaign {campaign_url}"
                logger.debug(error_msg)
            self.get_the_main_api_updated(
                campaign_api_url, running_campaigns_file, header_type, file_full_path
            )
