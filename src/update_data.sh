# This file could be used to download the remote data and updated campaigns
# It's useful to run as cron job
# You can get the path for poetry and python by using the `which` command. 
# e.g: `which poetry` and `poetry run which python`

poetry_path="$HOME/.local/bin/poetry"
project_dir="$HOME/flip_check/"
python_path="$project_dir/.venv/bin/python"
script_path="$project_dir/src/check_download_new.py"
last_check_file="$project_dir/last_check.txt"

$poetry_path run --directory $project_dir $python_path $script_path
dt=$(date '+%Y-%m-%dT%T.%zZ')
echo $dt >> $last_check_file
