from crontab import CronTab
import getpass
from pathlib import Path

home_dir = Path.home()
user = getpass.getuser()

cron = CronTab(user=user)
job = cron.new(command=f"/bin/bash {home_dir}/flip_check/src/update_data.sh")

job.minute.every(30)

cron.write()

print(job.enable())
