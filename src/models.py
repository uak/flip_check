from peewee import *
from datetime import datetime

import json
import logging


class Campaigns(Model):
    url = TextField(unique=True)
    campaign_file = TextField(unique=True)
    campaign_title = TextField(unique=True)
    requested_satoshi = IntegerField(null=True)
    start_date = DateTimeField()
    end_date = DateTimeField()
    created_date = DateTimeField(default=datetime.now)
    is_active = BooleanField(default=True, null=True)

    # ~ class Meta:
    # ~ database = db # This model uses the "my_database.db" database.


class Hashes(Model):
    file_hash = TextField(unique=True)
    campaign_id = ForeignKeyField(Campaigns, backref="tweets", unique=True)
    # ~ class Meta:
    # ~ database = db # This model uses the "my_database.db" database.


db = SqliteDatabase("db.sqlite3", pragmas={"foreign_keys": 1})


# ~ Hashes.create_table()
MODELS = [Campaigns, Hashes]

db.bind(MODELS, bind_refs=False, bind_backrefs=False)

db.create_tables([Campaigns, Hashes])


def save_campaign_info(url, campaign_file):
    """Checks if a Telegram user is present in the database.
    Returns True if a user is created, False otherwise.
    """
    db.connect(reuse_if_open=True)

    try:
        with open(campaign_file) as f:
            campaign_info = json.load(f)
    except Exception as e:
        logging.error(f"issue with opening campaign file - Exception: {e}")
    try:
        Campaigns.create(
            url=url,
            campaign_file=campaign_file,
            campaign_title=campaign_info["campaign"]["title"],
            requested_satoshi=campaign_info["recipients"][0]["recipient_satoshis"],
            start_date=campaign_info["campaign"]["starts"],
            end_date=campaign_info["campaign"]["expires"],
            created_date=datetime.now(),
        )
        db.close()
        return True
    except IntegrityError:
        db.close()
        return False
    except Exception as e:
        logging.error(f"Exception: {e}")


def save_hash(file_hash, campaign_id):
    """
    Store hash in database
    """
    db.connect(reuse_if_open=True)
    # ~ db.bind(MODELS, bind_refs=False, bind_backrefs=False)
    logging.info(f"save_hash called")
    logging.info(f"save_hash hash:{file_hash}, id={campaign_id}")
    try:
        Hashes.insert(
            file_hash=file_hash,
            campaign_id=campaign_id,
        ).on_conflict(
            conflict_target=(Hashes.campaign_id,),
            preserve=(Hashes.campaign_id),
            update={Hashes.file_hash: file_hash},
        ).execute()
        db.close()
        logging.info(f"hash stored in database")
        return True
    except IntegrityError as e:
        db.close()
        logging.error(f"IntegrityError {e}")
    except Exception as e:
        logging.error(f"Exception: {e}")


def get_local_hash(data_file_name):
    """
    Store hash in database
    """
    db.connect(reuse_if_open=True)
    db.bind(MODELS, bind_refs=False, bind_backrefs=False)
    try:
        campaign_id = Campaigns.get(Campaigns.campaign_file == data_file_name).id
        campaign = Campaigns.get(campaign_file=campaign_file_name)
        _hash = Hashes(campaign_file=campaign)
        logging.debug(f"_hash: {_hash}")
    except Exception as e:
        logging.error(f"Exception: {e}")
        raise e
