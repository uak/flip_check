from get import HandleRemoteAPI

from dotenv import load_dotenv

from pathlib import Path
import logging
import os

os.chdir(Path(__file__).parent)

logging.basicConfig(filename="example.log", encoding="utf-8", level=logging.DEBUG)

load_dotenv()

api_url = os.environ["api_url"]
data_file = os.environ["data_file"]
data_dir = os.environ["data_dir"]
quick_storage_file = os.environ["quick_storage_file"]
running_campaigns_file = os.environ["running_campaigns_file"]

Path(data_dir).mkdir(parents=True, exist_ok=True)

api_class = HandleRemoteAPI()


api_class.get_the_main_api_updated(
    api_url, quick_storage_file, "Content-Length", data_file
)

api_class.get_running_more_data(data_file, running_campaigns_file, "ETag")
