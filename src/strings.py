add_command_text = """
Currently you should submit the campaign manually using\n
https://flipstarters.bitcoincash.network/
"""

welcome_text = """Hi, I'm Flipstarter Fetch Bot!

**Commands:**
`/active` to list active campaigns
`/recent` to list recently funded campaigns
`/camp [number]` to get detailed campain info
`/add` to get info on adding a campgain to the list
`/create` to get info on launching your own campaign
Check [source code](https://gitlab.com/uak/flip_check)

🏗️ Bot is sitll under heavy development
"""

create_text = """
To create a campaign you have the following options:
• [Host your own campaign](https://flipstarter.cash/en/how-to) on DigitalOcean
• Hire someone like @Tech857 for ~$50
"""

extra_active_text = """
You can get more info by using `/camp #index`.
Example: `/camp 171`

Check the directory:
https://flipstarters.bitcoincash.network/#/active
"""

no_active_text = """No active campaigns!
Check the directory:
https://flipstarters.bitcoincash.network/#/active
"""
