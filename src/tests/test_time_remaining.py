# https://stackoverflow.com/a/54701179

import datetime
from time_remaining import countdown

diff_days = "31 days"
diff_hours = "11 h 6 m."

start_timestamp = int("1703809659")
end_timestamp = int("1706488059")
end_hour_timestamp = int("1703849625")


def test_countdown_days():
    remaining = countdown(start_timestamp, end_timestamp)
    assert remaining == diff_days


def test_countdown_hours():
    remaining = countdown(start_timestamp, end_hour_timestamp)
    assert remaining == diff_hours
