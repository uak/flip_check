import pytest
import json
import requests
import shelve
import time
from datetime import date
import os

from get import HandleRemoteAPI, get_by_key_value

test_api = "https://jsonplaceholder.typicode.com/users"
# ~ test_api2 = "http://ip.jsontest.com/"

fake_headers = {
    "Accept-Language": "en-US,en;q=0.8",
    "Host": "headers.jsontest.com",
    "Accept-Charset": "ISO-8859-1,utf-8;q=0.7,*;q=0.3",
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
}

api_class = HandleRemoteAPI()

_hash = "d8b0c12d745400e7aad48fe33c6a830547c3fcbd7750953aefe6e25557921f56"

json_file_hash = "cdc3997fe30edac1fc93a3c258ffb885ba84c37b0aacc7379f042c751fa5b703"

example_data = {"ip": "8.8.8.8"}

quick_storage_file = "storage.pickle"

json_file = "test.json"

test_dagur_campaigns = "tests/fake_dagur_campaigns_data.json"

test_running_campaigns_data = "tests/fake_running_campaigns.json"

test_successful_campaigns_file = "tests/fake_successful_campaigns.json"

test_expired_campaigns_file = "tests/fake_expired_campaigns.json"

test_running_campaigns_data_indexed = "tests/fake_running_campaigns_indexed.json"

test_successful_campaigns_file_indexed = "tests/fake_successful_campaigns_indexed.json"

test_expired_campaigns_file_indexed = "tests/fake_expired_campaigns_indexed.json"

last_5_test_file = "tests/fake_last_5_campaigns.json"

basic_info_file = "tests/fake_basic_info_file.md"
basic_info_expired_file = "tests/fake_basic_info_expired_file.md"


campaign_info_file = "tests/fake_info_file.md"


def test_url(requests_mock):
    requests_mock.get("http://test.com", json=example_data)
    assert example_data == requests.get("http://test.com").json()


def write_json_file(json_file):
    with open(json_file, "w+") as f:
        json.dump(example_data, f)


def test_get_remote_file(requests_mock):
    requests_mock.get(test_api, json=example_data)
    response = api_class.get_remote_file(test_api)
    assert response.status_code == 200


def test_get_hash():
    write_json_file(json_file)
    with open(json_file, "rb") as f:
        string_hash = api_class.get_hash(f.read())
    assert string_hash == json_file_hash


# ~ def test_save_hash():
    # ~ api_class.save_hash(quick_storage_file, json_file_hash)
    # ~ with shelve.open(quick_storage_file) as shelf:
        # ~ _hash = shelf['hash']
        # ~ _time = shelf['time']
        # ~ try:
            # ~ date.fromtimestamp(_time)
        # ~ except:
            # ~ raise ValueError
    # ~ assert json_file_hash == _hash


def test_get_remote_header(requests_mock):
    requests_mock.head(test_api, headers=fake_headers)
    headers = api_class.get_remote_header(test_api)
    assert headers.headers == fake_headers


def test_get_remote_file(requests_mock):
    requests_mock.get(test_api, json=example_data)
    response = api_class.get_remote_file(test_api)
    assert response.status_code == 200


# ~ def test_outdated_file():
    # ~ with shelve.open(quick_storage_file) as shelf:
        # ~ print("###\n", type(shelf))
        # ~ k,v = shelf.items()
        # ~ print(k,v)
        # ~ last_checked_time = shelf['time']
        # ~ logging.info(f"last_checked_time: {last_checked_time}")
# ~ assert api_class.outdated_file(quick_storage_file, 30, last_checked_time) == False


def test_load_local_data():
    data = api_class.load_local_data(json_file)
    assert data["ip"] == example_data["ip"]


def test_store_remote_json(requests_mock):
    open(json_file, "w").close()
    requests_mock.get(test_api, json=example_data)
    remote_data = api_class.get_remote_file(test_api)
    api_class.store_remote_json(json_file, remote_data)
    with open(json_file) as f:
        data = json.load(f)
    assert data["ip"] == example_data["ip"]


def test_compare_with_remote(requests_mock):
    requests_mock.get(test_api, content=json.dumps(example_data).encode("utf-8"))
    response = api_class.get_remote_file(test_api)
    remote_hash = api_class.get_hash(response.content)

    assert api_class.compare_with_remote(remote_hash, json_file_hash)


def test_get_running_campaigns():
    data = api_class.load_local_data(test_dagur_campaigns)
    running_campaign = api_class.load_local_data(test_running_campaigns_data)

    assert api_class.get_running_campaigns(data) == running_campaign


def test_get_successful_campaigns():
    data = api_class.load_local_data(test_dagur_campaigns)
    successful_campaigns = api_class.load_local_data(test_successful_campaigns_file)

    assert api_class.get_successful_campaigns(data) == successful_campaigns


def test_get_expired_campaigns():
    data = api_class.load_local_data(test_dagur_campaigns)
    expired_campaigns = api_class.load_local_data(test_expired_campaigns_file)

    assert api_class.get_expired_campaigns(data) == expired_campaigns


def test_get_running_campaigns_indexed():
    data = api_class.load_local_data(test_dagur_campaigns)
    indexed_data = api_class.index_data(data)
    running_campaigns = api_class.load_local_data(test_running_campaigns_data_indexed)

    assert api_class.get_running_campaigns_indexed(indexed_data) == running_campaigns


def test_get_successful_campaigns_indexed():
    data = api_class.load_local_data(test_dagur_campaigns)
    indexed_data = api_class.index_data(data)
    successful_campaigns = api_class.load_local_data(
        test_successful_campaigns_file_indexed
    )
    assert api_class.get_successful_campaigns_indexed(indexed_data) == successful_campaigns


def test_get_expired_campaigns_indexed():
    data = api_class.load_local_data(test_dagur_campaigns)
    indexed_data = api_class.index_data(data)
    expired_campaigns = api_class.load_local_data(test_expired_campaigns_file_indexed)

    assert api_class.get_expired_campaigns_indexed(indexed_data) == expired_campaigns


def test_get_basic_info_expired():
    campaigns = api_class.load_local_data(test_expired_campaigns_file)
    indexed_data = api_class.index_data(campaigns)
    test = api_class.get_basic_info(indexed_data, "expired")
    # ~ with open("test_777.txt", "w+") as f1:
        # ~ f1.write(test)    
    assert test == api_class.load_local_file(basic_info_expired_file)




def test_get2_basic_info():
    campaigns = api_class.load_local_data(test_dagur_campaigns)
    indexed_data = api_class.index_data(campaigns)
    test = api_class.get_basic_info(indexed_data, "general")
    # ~ with open("test_555.txt", "w+") as f1:
        # ~ f1.write(test)
    # ~ test2 = api_class.load_local_file(basic_info_file)
    # ~ with open("test_666.txt", "w+") as f2:
        # ~ f2.write(test2)
    # ~ print(test2)
    assert test == api_class.load_local_file(basic_info_file)

def test_get_info():
    campaigns = api_class.load_local_data(test_dagur_campaigns)
    indexed_data = api_class.index_data(campaigns)
    campaign_data = indexed_data[0]
    campaign_test_info = api_class.load_local_file(campaign_info_file)
    # Fix for empty line at the end of file failing the test by removing empty lines.
    campaign_test_info_ok = os.linesep.join([s for s in campaign_test_info.splitlines() if s])
    test = api_class.get_info(campaign_data)
    # ~ with open("test_000.txt", "w+") as f1:
        # ~ f1.write(test)
    assert api_class.get_info(campaign_data) == campaign_test_info_ok

def test_get_last_5():
    campaigns = api_class.load_local_data(test_dagur_campaigns)
    last_5_test = api_class.load_local_data(last_5_test_file)

    assert api_class.get_last_5(campaigns, 0, 4) == last_5_test
