✨ Campaigns ✨

🔥 [Add Multi-language support to Badger Android Mobile wallet](https://badger-localization.googol.cash/)                    
**💰 Requested:** 0.85                    
**🗂️ Index:** 0                    

🔥 [Knuth Node as a Javascript Library Campaign](https://fund.kth.cash/?id=11)                    
**💰 Requested:** 48.0                    
**🗂️ Index:** 1                    

🔥 [Cultura BCH Ryver Venezuela Organic Education and Adoption](http://flipstarter.ryver.site/)                    
**💰 Requested:** 31.68                    
**🗂️ Index:** 2                    

🔥 [Bitcoin Verde](https://verde.flipstarter.cash/)                    
**💰 Requested:** 241.0                    
**🗂️ Index:** 3                    

🔥 [bitcoincashj](https://flipstarter.pokkst.xyz/)                    
**💰 Requested:** 65.0                    
**🗂️ Index:** 4                    

🔥 [Bitcoin Cash (BCH) Marketing](https://flipstarter.bitcoinbch.com/)                    
**💰 Requested:** 650.0                    
**🗂️ Index:** 5                    

🔥 [BCH DevCon III](https://flipstarter.devcon.cash/)                    
**💰 Requested:** 30.0                    
**🗂️ Index:** 6                    

🔥 [BCHD](https://bchd.flipstarter.cash/)                    
**💰 Requested:** 360.0                    
**🗂️ Index:** 7                    

🔥 [Bitcoin Cash Node](https://bchn.flipstarter.cash/)                    
**💰 Requested:** 978.0                    
**🗂️ Index:** 8                    

🔥 [Integrate BCH at Bitfortip](http://138.68.81.43/)                    
**💰 Requested:** 8.0                    
**🗂️ Index:** 9                    

🔥 [member.cash flipstarter](http://flipstarter.member.cash/)                    
**💰 Requested:** 200.0                    
**🗂️ Index:** 10                    

🔥 [Help Mark Lundeberg work on Bitcoin Cash](https://bitcoincashfund.com/)                    
**💰 Requested:** 160.0                    
**🗂️ Index:** 11                    

🔥 [Electron Cash Flipstarter](https://flipstarter.electroncash.org/)                    
**💰 Requested:** 300.0                    
**🗂️ Index:** 12                    

🔥 [Bring Stamp Wallet to mobile and mainnet!](https://flipstarter.getstamp.io/)                    
**💰 Requested:** 300.0                    
**🗂️ Index:** 13                    

🔥 [Mitra Labs — Smart Contract System For Bitcoin Cash](https://mitra.be.cash/)                    
**💰 Requested:** 250.0                    
**🗂️ Index:** 14                    

🔥 [Bitcoin ABC](https://abc.flipstarter.cash/)                    
**💰 Requested:** 1000.0                    
**🗂️ Index:** 15                    

🔥 [Electro CashX: Wallet & DEX!](http://electrocashxfund.tech/)                    
**💰 Requested:** 65.0                    
**🗂️ Index:** 16                    

🔥 [KNUTH NODE AS A C#/.NET LIBRARY](https://campaigns.kth.cash/?id=10)                    
**💰 Requested:** 80.0                    
**🗂️ Index:** 17                    

🔥 [Venezuela Workers Fundraising](https://flipstarter.venezuelaworkers.net/)                    
**💰 Requested:** 102.0                    
**🗂️ Index:** 18                    

🔥 [Gameflame.cash | Flipstarter campaign](http://flipstarter.gameflame.cash/)                    
**💰 Requested:** 30.0                    
**🗂️ Index:** 19                    

🔥 [EatBCH: Helping those in need and increasing adoption](https://flipstarter.eatbch.org/)                    
**💰 Requested:** 117.4                    
**🗂️ Index:** 20                    

🔥 [BCH on CoinKit](https://flipstarter-coinkit.blockcurators.com/)                    
**💰 Requested:** 65.38                    
**🗂️ Index:** 21                    

🔥 [Bitcoin Cash (BCH) Marketing](https://flipstarter.bitcoinbch.com/)                    
**💰 Requested:** 360.0                    
**🗂️ Index:** 22                    

