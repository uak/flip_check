🔥 Campaign: [Add Multi-language support to Badger Android Mobile wallet](https://badger-localization.googol.cash/)                
**💰 Requested:** 0.85                
**🏳️ status:** running                
**🗒️** The developer will work on adding localization support to Badger Android mobile wallet so people can have string translated. Short and simple :).
**💾 Archives**: [1](https://archive.is/UtpZ2) 
**📢 Announcements**: [1](https://www.reddit.com/r/btc/comments/lg46kw/flipstarter_to_add_localization_support_to_badger/gmr5ol2/) 
