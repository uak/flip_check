import sys
import os

from dotenv import load_dotenv

load_dotenv()

api_url = os.environ["api_url"]
data_file = os.environ["data_file"]

sys.path.append(os.path.join(os.path.dirname(__file__), os.pardir))

from get import HandleRemoteAPI

test = HandleRemoteAPI()
remote_data = test.get_remote_file(api_url)

test.store_remote_json(data_file, remote_data)

data = test.load_local_data(data_file)

